<?php
namespace Gstarczyk\Mimic\TimesVerifiers;

class AtLeast extends AbstractVerifier
{
    public function verify($times)
    {
        if (!($times >= $this->getExpectedTimes())) {
            throw new TimesVerificationException(
                sprintf(
                    'Expected at least %d, but %d was given.',
                    $this->getExpectedTimes(),
                    $times
                )
            );
        }
    }
}