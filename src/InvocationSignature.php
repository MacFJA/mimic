<?php
namespace Gstarczyk\Mimic;

class InvocationSignature
{
    /** @var string */
    private $methodName;

    /** @var array */
    private $arguments;

    /**
     * @param string $methodName
     * @param array $arguments
     */
    public function __construct($methodName, array $arguments)
    {
        $this->methodName = $methodName;
        $this->arguments = $arguments;
    }

    /**
     * @return string
     */
    public function getMethodName()
    {
        return $this->methodName;
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }
}
