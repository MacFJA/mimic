<?php
namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\Mock\MockFactory;
use Gstarczyk\Mimic\Stub\Stub;

class MimicRegistry
{
    /** @var Stub[] */
    private $stubs = [];

    /** @var InvocationRegistry[] */
    private $invocationRegistries = [];

    /** @var MockFactory */
    private $mockFactory;

    public function __construct(MockFactory $mockFactory)
    {
        $this->mockFactory = $mockFactory;
    }

    /**
     * @param string $className
     *
     * @return object
     */
    public function getMock($className)
    {
        $invocationRegistry = new InvocationRegistry();
        $stub = new Stub($invocationRegistry);
        $invocationHandler = new InvocationHandler(
            $invocationRegistry, $stub
        );
        $mock = $this->mockFactory->createMock($className, $invocationHandler);
        $this->registerInvocationRegistry($mock, $invocationRegistry);
        $this->registerStub($mock, $stub);

        return $mock;
    }

    /**
     * @param object $mock
     *
     * @return Stub
     */
    public function getStub($mock)
    {
        $className = get_class($mock);
        if (!array_key_exists($className, $this->stubs)) {
            throw new \RuntimeException(sprintf('No stub registered for mock "%s"', $className));
        }

        return $this->stubs[$className];
    }

    /**
     * @param object $mock
     *
     * @return InvocationRegistry
     */
    public function getInvocationRegistry($mock)
    {
        $className = get_class($mock);
        if (!array_key_exists($className, $this->invocationRegistries)) {
            throw new \RuntimeException(sprintf('No invocation registry registered for mock "%s"', $className));
        }

        return $this->invocationRegistries[$className];
    }

    /**
     * @param object $mock
     * @param Stub $stub
     *
     * @return void
     */
    private function registerStub($mock, Stub $stub)
    {
        $className = get_class($mock);
        $this->stubs[$className] = $stub;
    }

    /**
     * @param object $mock
     * @param InvocationRegistry $invocationRegistry
     *
     * @return void
     */
    private function registerInvocationRegistry($mock, InvocationRegistry $invocationRegistry)
    {
        $className = get_class($mock);
        $this->invocationRegistries[$className] = $invocationRegistry;
    }
}