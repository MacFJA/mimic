<?php
namespace Gstarczyk\Mimic\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatcher;

class EmptyArguments implements ArgumentsMatcher
{
    public function match(array $arguments)
    {
        return count($arguments) == 0;
    }
}