<?php
namespace Gstarczyk\Mimic\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\ValueMatcher;

class MatchingValues implements ArgumentsMatcher
{
    /** @var ValueMatcher[] */
    private $matchers = [];

    public function __construct(array $matchers)
    {
        foreach ($matchers as $matcher) {
            $this->appendMatcher($matcher);
        }
    }

    private function appendMatcher(ValueMatcher $matcher)
    {
        $this->matchers[] = $matcher;
    }

    public function match(array $arguments)
    {
        if (!$this->validateSize($arguments)) {
            return false;
        }
        $values = array_values($arguments);

        foreach ($values as $key => $value) {
            if (!$this->matchers[$key]->match($value)) {
                return false;
            }
        }

        return true;
    }

    private function validateSize(array $arguments)
    {
        return count($this->matchers) == count($arguments);
    }
}