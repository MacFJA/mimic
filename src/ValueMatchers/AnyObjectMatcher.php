<?php
namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class AnyObjectMatcher implements ValueMatcher
{
    /** @var string */
    private $className;

    /**
     * @param string $className
     */
    public function __construct($className = null)
    {
        $this->className = $className;
    }

    public function match($value)
    {
        if (!is_object($value)) {
            return false;
        }
        if ($this->className !== null) {
            return $value instanceof $this->className;
        }

        return true;
    }
}