<?php
namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class StringStartsWith implements ValueMatcher
{
    private $prefix;

    /**
     * @param mixed $prefix
     */
    public function __construct($prefix)
    {
        $this->prefix = $prefix;
    }

    public function match($value)
    {
        return mb_strpos($value, $this->prefix) === 0;
    }
}