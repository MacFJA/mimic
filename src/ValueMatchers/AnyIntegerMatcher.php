<?php
namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class AnyIntegerMatcher implements ValueMatcher
{
    public function match($value)
    {
        return is_int($value);
    }
}