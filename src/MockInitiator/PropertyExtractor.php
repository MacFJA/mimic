<?php
namespace Gstarczyk\Mimic\MockInitiator;

use InvalidArgumentException;
use ReflectionClass;

class PropertyExtractor
{
    /** @var ObjectPropertyFactory */
    private $propertyFactory;

    /** @var ContextFactory */
    private $contextFactory;

    public function __construct(ObjectPropertyFactory $propertyFactory, ContextFactory $contextFactory)
    {
        $this->propertyFactory = $propertyFactory;
        $this->contextFactory = $contextFactory;
    }

    /**
     * @param object $parentObject
     * @param string | null $pathToParentObjectFile
     *
     * @return ObjectProperty[]
     */
    public function extractProperties($parentObject, $pathToParentObjectFile = null)
    {
        $this->validateParentObject($parentObject);
        $context = $this->contextFactory->createContext($parentObject, $pathToParentObjectFile);
        $result = [];
        $class = new ReflectionClass($parentObject);
        $properties = $class->getProperties();
        foreach ($properties as $property) {
            $result[] = $this->propertyFactory->createObjectProperty($property, $context);
        }

        return $result;
    }

    private function validateParentObject($parentObject)
    {
        if (!is_object($parentObject)) {
            throw new InvalidArgumentException(
                sprintf('Parent object must be an object instance, %s was given.', gettype($parentObject))
            );
        }
    }
}
