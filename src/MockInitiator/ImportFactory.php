<?php
namespace Gstarczyk\Mimic\MockInitiator;

class ImportFactory
{
    /**
     * @param string $fullClassName
     * @param string | null $alias
     * @return Import
     */
    public function createImport($fullClassName, $alias = null)
    {
        $tmp = explode('\\', $fullClassName);
        $shortClassName = array_pop($tmp);
        $namespace = '\\' . trim(implode('\\', $tmp), '\\');

        $import = new Import($namespace, $shortClassName, $alias);

        return $import;
    }
}