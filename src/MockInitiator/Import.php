<?php
namespace Gstarczyk\Mimic\MockInitiator;

class Import
{
    /** @var string */
    private $namespace;

    /** @var string */
    private $shortClassName;

    /** @var string */
    private $alias;

    /**
     * @param string $namespace
     * @param string $shortClassName
     * @param string | null $alias
     */
    public function __construct($namespace, $shortClassName, $alias = null)
    {
        $this->namespace = $namespace;
        $this->shortClassName = $shortClassName;
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getFullClassName()
    {
        return sprintf('%s\%s', $this->namespace, $this->shortClassName);
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @return string
     */
    public function getShortClassName()
    {
        return $this->shortClassName;
    }

    /**
     * @return string | null
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @return bool
     */
    public function hasAlias()
    {
        return is_string($this->alias);
    }
}
