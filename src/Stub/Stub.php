<?php
namespace Gstarczyk\Mimic\Stub;

use Gstarczyk\Mimic\InvocationCounter;
use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Actions\DoNothing;

class Stub
{
    /** @var InvocationCounter */
    private $invocationCounter;

    /** @var Behaviour[] */
    private $behaviours = [];

    public function __construct(InvocationCounter $invocationCounter)
    {
        $this->invocationCounter = $invocationCounter;
    }

    /**
     * @param InvocationSignature $invocationSignature
     * @return Action
     */
    public function findAction(InvocationSignature $invocationSignature)
    {
        foreach ($this->behaviours as $behaviour) {
            if ($this->match($behaviour, $invocationSignature)) {
                return $behaviour->getAction();
            }
        }

        return new DoNothing();
    }

    public function registerBehaviour(Behaviour $behaviour)
    {
        $this->behaviours[] = $behaviour;
    }

    /**
     * @param Behaviour $behaviour
     * @param InvocationSignature $invocationSignature
     * @return bool
     */
    private function match(Behaviour $behaviour, InvocationSignature $invocationSignature)
    {
        $invocationMatcher = $behaviour->getMatcher();
        $result = false;
        if ($invocationMatcher->match($invocationSignature)) {
            $result = true;
            if ($behaviour->hasInvocationNumber()) {
                $count = $this->invocationCounter->invocationCount($invocationMatcher);
                $result = $behaviour->getInvocationNumber() == $count;
            }
        }

        return $result;
    }
}