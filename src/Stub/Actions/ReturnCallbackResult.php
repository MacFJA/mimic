<?php
namespace Gstarczyk\Mimic\Stub\Actions;

use Closure;
use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Action;

class ReturnCallbackResult implements Action
{
    /** @var Closure */
    private $callback;

    public function __construct(Closure $callback)
    {
        $this->callback = $callback;
    }

    public function perform(InvocationSignature $invocationSignature)
    {
        $callback = $this->callback;

        return call_user_func_array($callback, $invocationSignature->getArguments());
    }
}