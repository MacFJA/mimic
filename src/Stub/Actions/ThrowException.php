<?php
namespace Gstarczyk\Mimic\Stub\Actions;

use Exception;
use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Action;

class ThrowException implements Action
{
    /** @var Exception */
    private $exception;

    public function __construct(Exception $exception)
    {
        $this->exception = $exception;
    }

    public function perform(InvocationSignature $invocationSignature)
    {
        throw $this->exception;
    }
}