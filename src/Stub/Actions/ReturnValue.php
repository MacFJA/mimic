<?php
namespace Gstarczyk\Mimic\Stub\Actions;

use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Action;

class ReturnValue implements Action
{
    /** @var mixed */
    private $value;

    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function perform(InvocationSignature $invocationSignature)
    {
        return $this->value;
    }
}