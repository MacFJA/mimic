<?php
namespace Gstarczyk\Mimic;

interface ValueMatcher
{
    /**
     * @param mixed $value
     * @return bool
     */
    public function match($value);
}