<?php
namespace Gstarczyk\Mimic\Mock;

use RuntimeException;

class ClassDefinition
{
    /** @var  string */
    private $namespace;

    /** @var  string */
    private $shortName;

    /** @var  string */
    private $extends;

    /** @var string */
    private $implements;

    /** @var ClassAttributeDefinition[] */
    private $attributes = [];

    /** @var MethodDefinition[] */
    private $methodDefinitions = [];

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->namespace . '\\' . $this->shortName;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param string $namespace
     */
    public function setNamespace($namespace)
    {
        $this->namespace = trim($namespace, '\\');
    }

    /**
     * @return string
     */
    public function getExtends()
    {
        return $this->extends;
    }

    /**
     * @param string $extends
     */
    public function setExtends($extends)
    {
        $this->extends = trim($extends, '\\');
    }

    /**
     * @return string
     */
    public function getImplements()
    {
        return $this->implements;
    }

    /**
     * @param string $implements
     */
    public function setImplements($implements)
    {
        $this->implements = trim($implements, '\\');
    }

    /**
     * @return ClassAttributeDefinition[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param ClassAttributeDefinition[] $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = [];
        foreach ($attributes as $attribute) {
            $this->addAttribute($attribute);
        }
    }

    public function addAttribute(ClassAttributeDefinition $attributeDefinition)
    {
        $this->attributes[] = $attributeDefinition;
    }

    /**
     * @return MethodDefinition[]
     */
    public function getMethodDefinitions()
    {
        return $this->methodDefinitions;
    }

    /**
     * @param string $methodName
     * @return MethodDefinition
     * @throws RuntimeException
     */
    public function getMethodDefinition($methodName)
    {
        foreach ($this->methodDefinitions as $methodDefinition) {
            if ($methodDefinition->getMethodName() == $methodName) {
                return $methodDefinition;
            }
        }

        throw new RuntimeException(sprintf('Unknown method [name="%s"]', $methodName));
    }

    public function addMethodDefinition(MethodDefinition $methodDefinition)
    {
        $this->methodDefinitions[] = $methodDefinition;
    }

    /**
     * @return string
     */
    public function toCode()
    {
        $code = '';
        if ($this->namespace) {
            $code .= 'namespace ' . $this->namespace . ';' . PHP_EOL;
        }
        $code .= 'class ' . $this->shortName;
        if ($this->extends) {
            $code .= ' extends \\' . $this->extends;
        } elseif ($this->implements) {
            $code .= ' implements \\' . $this->implements;
        }
        $code .= PHP_EOL;
        $code .= '{' . PHP_EOL;
        foreach ($this->attributes as $attribute) {
            $code .= $attribute->toCode() . PHP_EOL;
        }
        foreach ($this->methodDefinitions as $methodDefinition) {
            $code .= $methodDefinition->toCode() . PHP_EOL;
        }
        $code .= '}';

        return $code;
    }
}
