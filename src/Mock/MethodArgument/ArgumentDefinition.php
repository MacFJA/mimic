<?php
namespace Gstarczyk\Mimic\Mock\MethodArgument;

abstract class ArgumentDefinition
{
    /** @var  string */
    private $name;

    /** @var bool */
    private $optional = false;

    /** @var bool */
    private $passedByReference = false;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->setName($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function isOptional()
    {
        return $this->optional;
    }

    public function makeOptional()
    {
        $this->optional = true;
    }

    /**
     * @return boolean
     */
    public function isPassedByReference()
    {
        return $this->passedByReference;
    }

    public function makePassedByReference()
    {
        $this->passedByReference = true;
    }

    public function makePassedByValue()
    {
        $this->passedByReference = false;
    }

    /**
     * @return string
     */
    abstract public function toCode();
}