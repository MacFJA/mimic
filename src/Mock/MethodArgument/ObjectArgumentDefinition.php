<?php
namespace Gstarczyk\Mimic\Mock\MethodArgument;

class ObjectArgumentDefinition extends ArgumentDefinition
{
    /** @var  string */
    private $className;

    /**
     * @param string $name
     * @param string $className
     */
    public function __construct($name, $className)
    {
        parent::__construct($name);
        $this->className = $className;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function toCode()
    {
        $code = sprintf('\\%s %s$%s',
            $this->className,
            ($this->isPassedByReference() ? '&' : ''),
            $this->getName()
        );
        if ($this->isOptional()) {
            $code .= ' = null';
        }

        return $code;
    }
}