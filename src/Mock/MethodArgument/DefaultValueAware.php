<?php
namespace Gstarczyk\Mimic\Mock\MethodArgument;

interface DefaultValueAware
{
    /**
     * @param mixed $defaultValue
     * @return void
     */
    public function setDefaultValue($defaultValue);

    /**
     * @return mixed
     */
    public function getDefaultValue();
}