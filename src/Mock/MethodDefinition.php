<?php
namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\Mock\MethodArgument\ArgumentDefinition;

class MethodDefinition
{
    /** @var  string */
    private $methodName;

    /** @var ArgumentDefinition[] */
    private $arguments = [];

    /** @var array */
    private $instructions = [];

    /**
     * @return string
     */
    public function getMethodName()
    {
        return $this->methodName;
    }

    /**
     * @param string $methodName
     */
    public function setMethodName($methodName)
    {
        $this->methodName = $methodName;
    }

    /**
     * @return array
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * @param array $instructions
     */
    public function setInstructions(array $instructions)
    {
        $this->instructions = $instructions;
    }

    public function addInstruction($instruction)
    {
        $this->instructions[] = $instruction;
    }

    /**
     * @return ArgumentDefinition[]
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param ArgumentDefinition[] $arguments
     */
    public function setArguments($arguments)
    {
        $this->arguments = [];
        foreach ($arguments as $argument) {
            $this->addArgument($argument);
        }
    }

    /**
     * @param ArgumentDefinition $argument
     */
    public function addArgument($argument)
    {
        $this->arguments[] = $argument;
    }

    /**
     * @return string
     */
    public function toCode()
    {
        $arguments = [];
        foreach ($this->arguments as $argument) {
            $arguments[] = $argument->toCode();
        }
        $code = sprintf('public function %s(%s)',
                $this->methodName,
                implode(', ', $arguments)) . PHP_EOL;
        $code .= '{' . PHP_EOL;
        foreach ($this->instructions as $instruction) {
            $code .= $instruction . ';' . PHP_EOL;
        }
        $code .= '}';

        return $code;
    }
}