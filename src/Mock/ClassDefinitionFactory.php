<?php
namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\InvocationHandler;
use Gstarczyk\Mimic\Mock\MethodArgument\ArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\UniqueIdGenerator;
use InvalidArgumentException;
use ReflectionMethod;

class ClassDefinitionFactory
{
    /** @var  UniqueIdGenerator */
    private $uniqueIdGenerator;

    /** @var MethodArgumentFactory */
    private $methodArgumentFactory;

    public function __construct(UniqueIdGenerator $uniqueIdGenerator, MethodArgumentFactory $methodArgumentFactory)
    {
        $this->uniqueIdGenerator = $uniqueIdGenerator;
        $this->methodArgumentFactory = $methodArgumentFactory;
    }

    /**
     * @param string $className
     * @return ClassDefinition
     * @throws InvalidArgumentException when given class cannot be mocked, for ex. class is final
     */
    public function createClassDefinition($className)
    {
        $this->validateClassName($className);
        $class = new \ReflectionClass($className);
        if ($class->isFinal()) {
            throw new InvalidArgumentException(sprintf('Cannot mock final class [className=%s]', $class->getName()));
        }

        $imitationClassName = $class->getShortName() . '_' . $this->uniqueIdGenerator->generateId();
        $definition = new ClassDefinition();
        $definition->setShortName($imitationClassName);
        $definition->setNamespace($class->getNamespaceName());
        $definition->addAttribute(new ClassAttributeDefinition('__invocationHandler'));
        $definition->addMethodDefinition($this->createConstructorDefinition());

        if ($class->isInterface()) {
            $definition->setImplements($class->getName());
        } else {
            $definition->setExtends($class->getName());
        }

        foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if ($method->getShortName() !== '__construct' && !$method->isStatic()) {
                $definition->addMethodDefinition($this->createMethodDefinition($method));
            }
        }

        return $definition;
    }

    /**
     * @param ReflectionMethod $method
     * @return MethodDefinition
     */
    private function createMethodDefinition(ReflectionMethod $method)
    {
        if ($method->isFinal()) {
            throw new InvalidArgumentException(sprintf('Cannot mock final method [methodName=%s]', $method->getName()));
        }
        $definition = new MethodDefinition();
        $definition->setArguments($this->createArgumentsList($method));
        $definition->setMethodName($method->getShortName());
        $definition->setInstructions([
            'return $this->__invocationHandler->handleInvocation(__FUNCTION__, func_get_args())',
        ]);

        return $definition;
    }

    /**
     * @return MethodDefinition
     */
    private function createConstructorDefinition()
    {
        $definition = new MethodDefinition();
        $definition->setArguments([
            new ObjectArgumentDefinition('invocationHandler', InvocationHandler::class),
        ]);
        $definition->setMethodName('__construct');
        $definition->setInstructions([
            '$this->__invocationHandler = $invocationHandler',
        ]);

        return $definition;
    }

    /**
     * @param ReflectionMethod $method
     * @return ArgumentDefinition[]
     */
    private function createArgumentsList(ReflectionMethod $method)
    {
        $arguments = [];
        foreach ($method->getParameters() as $parameter) {
            $arguments[] = $this->methodArgumentFactory->createArgument($parameter);
        }

        return $arguments;
    }

    /**
     * @param string $type
     * @throws InvalidArgumentException
     */
    private function validateClassName($type)
    {
        if (!class_exists($type) && !interface_exists($type)) {
            throw new InvalidArgumentException(sprintf(
                'Cannot create mock for "%s"',
                $type
            ));
        }
    }
}