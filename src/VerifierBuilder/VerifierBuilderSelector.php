<?php
namespace Gstarczyk\Mimic\VerifierBuilder;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;
use Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;

class VerifierBuilderSelector
{
    /** @var InvocationRegistry */
    private $invocationRegistry;

    /** @var ArgumentsMatcherFactory */
    private $argumentsMatcherFactory;

    public function __construct(InvocationRegistry $invocationRegistry, ArgumentsMatcherFactory $matchingArgumentsFactory)
    {
        $this->invocationRegistry = $invocationRegistry;
        $this->argumentsMatcherFactory = $matchingArgumentsFactory;
    }

        /**
     * @param string $methodName
     * @return FilteredInvocations\ArgumentsChooser
     */
    public function method($methodName)
    {
        $verifier = new FilteredInvocations\VerifierBuilder(
            $this->invocationRegistry,
            $this->argumentsMatcherFactory
        );
        $verifier->method($methodName);

        return $verifier;
    }

    /**
     * @param string $methodName
     * @return ConsecutiveInvocations\FirstInvocationArguments
     */
    public function consecutiveMethodInvocations($methodName)
    {
        $verifier = new ConsecutiveInvocations\VerifierBuilder(
            $this->invocationRegistry,
            $this->argumentsMatcherFactory
        );
        $verifier->setMethodName($methodName);


        return $verifier;
    }
}