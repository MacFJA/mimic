<?php
namespace Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;

interface ArgumentsChooser
{
    /**
     * @param $_
     * @return Verification
     */
    public function with($_);

    /**
     * @return Verification
     */
    public function withAnyArguments();

    /**
     * @return Verification
     */
    public function withoutArguments();
}