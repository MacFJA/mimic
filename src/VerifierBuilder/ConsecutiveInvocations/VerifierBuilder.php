<?php
namespace Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\ArgumentsMatchers\AnyArguments;
use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\ArgumentsMatchers\EmptyArguments;
use Gstarczyk\Mimic\InvocationRegistry;

class VerifierBuilder implements FirstInvocationArguments, ConsecutiveInvocationArguments
{
    /** @var InvocationRegistry */
    private $invocationRegistry;

    /** @var string */
    private $methodName;

    /** @var ArgumentsMatcherFactory */
    private $argumentsMatcherFactory;

    /** @var int */
    private $invocationNo = 0;

    public function __construct(
        InvocationRegistry $invocationRegistry,
        ArgumentsMatcherFactory $argumentsMatcherFactory
    ) {
        $this->invocationRegistry = $invocationRegistry;
        $this->argumentsMatcherFactory = $argumentsMatcherFactory;
    }

    /**
     * @param string $methodName
     */
    public function setMethodName($methodName)
    {
        $this->methodName = $methodName;
    }

    public function wasCalledWith($_)
    {
        $argumentsMatcher = $this->argumentsMatcherFactory->createMatcher(func_get_args());
        $this->verify($argumentsMatcher);
        ++$this->invocationNo;

        return $this;
    }

    public function wasCalledWithAnyArguments()
    {
        $this->verify(new AnyArguments());
        ++$this->invocationNo;

        return $this;
    }

    public function wasCalledWithoutArguments()
    {
        $this->verify(new EmptyArguments());
        ++$this->invocationNo;

        return $this;
    }

    public function thenWith($_)
    {
        return call_user_func_array([$this, 'wasCalledWith'], func_get_args());
    }

    public function thenWithAnyArguments()
    {
        return $this->wasCalledWithAnyArguments();
    }

    public function thenWithoutArguments()
    {
        return $this->wasCalledWithoutArguments();
    }

    /**
     * @param ArgumentsMatcher $argumentsMatcher
     * @throws VerificationException when verification fails
     */
    private function verify(ArgumentsMatcher $argumentsMatcher)
    {
        $invocations = $this->invocationRegistry->getMethodInvocations($this->methodName);

        if (!array_key_exists($this->invocationNo, $invocations)) {
            throw new VerificationException(
                sprintf('There was no expected invocation #%u of method "%s"', $this->invocationNo, $this->methodName)
            );
        }
        $invocation = $invocations[$this->invocationNo];
        if (!$argumentsMatcher->match($invocation->getArguments())) {
            throw new VerificationException(
                sprintf(
                    'Expectations for arguments of invocation #%u of method "%s" are not fulfilled',
                    $this->invocationNo,
                    $this->methodName
                )
            );
        }
    }
}
