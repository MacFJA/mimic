<?php
namespace Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;

interface FirstInvocationArguments
{
    /**
     * @param $_
     * @return ConsecutiveInvocationArguments
     */
    public function wasCalledWith($_);

    /**
     * @return ConsecutiveInvocationArguments
     */
    public function wasCalledWithAnyArguments();

    /**
     * @return ConsecutiveInvocationArguments
     */
    public function wasCalledWithoutArguments();
}