<?php
namespace Gstarczyk\Mimic\Examples;

require_once '../vendor/autoload.php';

use Gstarczyk\Mimic\Mimic;
use Gstarczyk\Mimic\Times;

/** @var One $mock */
$mock = Mimic::mock(One::class);
Mimic::when($mock)
    ->invoke('getSome')
    ->withoutArguments()
    ->willReturn(300);

var_dump($mock->getSome());

Mimic::when($mock)
    ->invoke('compute')
    ->with(10, 10)
    ->willReturn(20);

Mimic::when($mock)
    ->invoke('compute')
    ->with(20, 30)
    ->willReturn(50);

Mimic::when($mock)
    ->invoke('compute')
    ->withAnyArguments()
    ->willReturn(100);

var_dump($mock->compute(10,10));
var_dump($mock->compute(20,30));
var_dump($mock->compute(120,130));


$mock->getMore();
$mock->getMore();
$mock->getMore();

Mimic::verify($mock)
    ->method('getMore')
    ->withAnyArguments()
    ->wasCalled(Times::exactly(3));

Mimic::when($mock)
    ->consecutiveInvoke('getMore')
    ->willReturn('a')
    ->thenReturn('a')
    ->thenReturn('b')
    ->thenThrow(new \RuntimeException(''));

Mimic::verify($mock)
    ->consecutiveMethodInvocations('getMore')
    ->wasCalledWith('a')
    ->thenWith('b')
    ->thenWithAnyArguments()
    ->thenWithoutArguments()
    ->thenWith('c');

class One
{
    public function getSome()
    {
        return 100;
    }

    public function getMore()
    {
        return 1000;
    }

    public function compute($arg1, $arg2)
    {
        return $arg1 + $arg2;
    }
}

function atLeast($times)
{

}