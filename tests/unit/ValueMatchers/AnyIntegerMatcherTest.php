<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\AnyIntegerMatcher;

class AnyIntegerMatcherTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @param int $value
     * @dataProvider matchingValueProvider
     */
    public function testMatcherReturnTrueWhenGivenValueIsInteger($value)
    {
        $matcher = new AnyIntegerMatcher();
        $result = $matcher->match($value);

        $this->assertTrue($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsNotInteger()
    {
        $matcher = new AnyIntegerMatcher();
        $value = '100';
        $result = $matcher->match($value);

        $this->assertFalse($result);
    }

    public function matchingValueProvider()
    {
        return [
            'decimal' => [100],
            'negative decimal' => [-256],
            'octal' => [0123],
            'hexadecimal' => [0x1A],
            'binary' => [0b11111111],
        ];
    }

}
