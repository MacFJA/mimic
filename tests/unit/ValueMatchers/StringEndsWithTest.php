<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\StringEndsWith;
use PHPUnit_Framework_Assert as Assert;

class StringEndsWithTest extends \PHPUnit_Framework_TestCase
{
    public function testMatcherReturnTrueWhenGivenValueEndsWithSpecifiedString()
    {
        $specification = 'Bar';
        $value = 'fooBar';
        $matcher = new StringEndsWith($specification);
        $result = $matcher->match($value);

        Assert::assertTrue($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsNotEndsWithSpecifiedString()
    {
        $specification = 'foo';
        $value = 'fooBar';
        $matcher = new StringEndsWith($specification);
        $result = $matcher->match($value);

        Assert::assertFalse($result);
    }
}
