<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\SameMatcher;

class SameMatcherTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param mixed $specification
     * @param mixed $value
     * @dataProvider matchingValuesProvider
     */
    public function testMatcherReturnTrueWhenGivenValueIsSameAsSpecified($specification, $value)
    {
        $matcher = new SameMatcher($specification);
        $result = $matcher->match($value);

        $this->assertTrue($result);
    }

    /**
     * @param mixed $specification
     * @param mixed $value
     * @dataProvider notMatchingValuesProvider
     */
    public function testMatcherReturnFalseWhenGivenValueIsNotSameAsSpecified($specification, $value)
    {
        $matcher = new SameMatcher($specification);
        $result = $matcher->match($value);

        $this->assertFalse($result);
    }

    public function matchingValuesProvider()
    {
        $object = new \stdClass();

        return [
            'text' => ['some text', 'some text'],
            'integer' => [100, 100],
            'float' => [100.5, 100.5],
            'array' => [['some text', 256], ['some text', 256]],
            'object' => [$object, $object],
        ];
    }

    public function notMatchingValuesProvider()
    {
        return [
            'text' => ['some text', 'other text'],
            'integer' => [200, 100],
            'float' => [150.5, 100.5],
            'array' => [['some text'], ['some text', 256]],
            'object' => [new \stdClass(), new \stdClass()],
        ];
    }
}
