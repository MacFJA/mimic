<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\EqualMatcher;

class EqualMatcherTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param mixed $specification
     * @param mixed $value
     * @dataProvider matchingValuesProvider
     */
    public function testMatcherReturnTrueWhenGivenValueIsEqualWithSpecified($specification, $value)
    {
        $matcher = new EqualMatcher($specification);
        $result = $matcher->match($value);

        $this->assertTrue($result);
    }

    /**
     * @param mixed $specification
     * @param mixed $value
     * @dataProvider notMatchingValuesProvider
     */
    public function testMatcherReturnFalseWhenGivenValueIsNotEqualWithSpecified($specification, $value)
    {
        $matcher = new EqualMatcher($specification);
        $result = $matcher->match($value);

        $this->assertFalse($result);
    }

    public function matchingValuesProvider()
    {
        $object1 = new \stdClass();
        $object1->prop1 = 'some text';

        $object2 = new \stdClass();
        $object2->prop1 = 'some text';

        return [
            'text' => ['some text', 'some text'],
            'integer' => [100, 100],
            'float' => [100.5, 100.5],
            'array' => [['some text', 256], ['some text', 256]],
            'object' => [$object1, $object2],
        ];
    }

    public function notMatchingValuesProvider()
    {
        $object1 = new \stdClass();
        $object1->prop1 = 'some text';

        $object2 = new \stdClass();

        return [
            'text' => ['some text', 'other text'],
            'integer' => [200, 100],
            'float' => [150.5, 100.5],
            'array' => [['some text'], ['some text', 256]],
            'object' => [$object1, $object2],
        ];
    }
}
