<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\AnyObjectMatcher;

class AnyObjectMatcherTest extends \PHPUnit_Framework_TestCase
{
    public function testMatcherReturnTrueWhenGivenValueIsObject()
    {
        $matcher = new AnyObjectMatcher();
        $value = new \stdClass();
        $result = $matcher->match($value);

        $this->assertTrue($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsNotObject()
    {
        $matcher = new AnyObjectMatcher();
        $value = [];
        $result = $matcher->match($value);

        $this->assertFalse($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsObjectButNotInstanceOfSpecifiedClass()
    {
        $matcher = new AnyObjectMatcher(\ArrayObject::class);
        $value = new \stdClass();
        $result = $matcher->match($value);

        $this->assertFalse($result);
    }
}
