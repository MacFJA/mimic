<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\AnyTraversableMatcher;

class AnyTraversableMatcherTest extends \PHPUnit_Framework_TestCase
{
    /** @var AnyTraversableMatcher */
    private $matcher;

    protected function setUp()
    {
        $this->matcher = new AnyTraversableMatcher();
    }

    /**
     * @param int $value
     * @dataProvider matchingValueProvider
     */
    public function testMatcherReturnTrueWhenGivenValueIsTraversable($value)
    {
        $result = $this->matcher->match($value);

        $this->assertTrue($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsNotTraversable()
    {
        $value = '100';
        $result = $this->matcher->match($value);

        $this->assertFalse($result);
    }

    public function matchingValueProvider()
    {
        return [
            'array' => [[]],
            'iterator' => [new \ArrayIterator([])],
        ];
    }
}
