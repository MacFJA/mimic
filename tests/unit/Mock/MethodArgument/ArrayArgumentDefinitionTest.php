<?php
namespace Gstarczyk\Mimic\UnitTest\Mock\MethodArgument;

use Gstarczyk\Mimic\Mock\MethodArgument\ArrayArgumentDefinition;
use PHPUnit_Framework_Assert as Assert;

class ArrayArgumentDefinitionTest extends \PHPUnit_Framework_TestCase
{
    public function testToCodeReturnArgumentCodeAsString()
    {
        $argument = new ArrayArgumentDefinition('someArray');
        Assert::assertEquals('array $someArray', $argument->toCode());
    }

    public function testToCodeReturnArgumentCodeWithNullDefaultValueForOptionalArgument()
    {
        $argument = new ArrayArgumentDefinition('someArray');
        $argument->makeOptional();
        Assert::assertEquals('array $someArray = null', $argument->toCode());
    }

    public function testToCodeReturnArgumentCodeWithDefaultValueIfSet()
    {
        $argument = new ArrayArgumentDefinition('someArray');
        $argument->setDefaultValue([100, 'text']);
        Assert::assertEquals('array $someArray = [100, \'text\']', $argument->toCode());
    }

    public function testDefaultValueHasHigherPriorityThatNullableFlag()
    {
        $argument = new ArrayArgumentDefinition('someArray');
        $argument->setDefaultValue([]);
        $argument->makeOptional();
        Assert::assertEquals('array $someArray = []', $argument->toCode());
    }

    public function testToCodeWithPassedAsReference()
    {
        $argument = new ArrayArgumentDefinition('someArray');
        $argument->makePassedByReference();
        Assert::assertEquals('array &$someArray', $argument->toCode());
    }
}
