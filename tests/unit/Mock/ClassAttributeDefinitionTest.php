<?php
namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\Mock\ClassAttributeDefinition;
use PHPUnit_Framework_Assert as Assert;

class ClassAttributeDefinitionTest extends \PHPUnit_Framework_TestCase
{
    public function testToCodeProduceCodeDefinition()
    {
        $attribute = new ClassAttributeDefinition('attributeName');
        $expected = 'private $attributeName;';

        Assert::assertEquals($expected, $attribute->toCode());

    }
}
