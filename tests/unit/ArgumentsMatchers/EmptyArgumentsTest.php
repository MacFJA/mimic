<?php
namespace Gstarczyk\Mimic\UnitTest\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatchers\EmptyArguments;
use PHPUnit_Framework_Assert as Assert;

class EmptyArgumentsTest extends \PHPUnit_Framework_TestCase
{
    public function testMatcherReturnTrueForEmptyArguments()
    {
        $matcher = new EmptyArguments();

        $result = $matcher->match([]);

        Assert::assertTrue($result);
    }
}
