<?php
namespace Gstarczyk\Mimic\UnitTest\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsCaptor;
use PHPUnit_Framework_Assert as Assert;

class ArgumentsCaptorTest extends \PHPUnit_Framework_TestCase
{
    /** @var ArgumentsCaptor */
    private $matcher;

    protected function setUp()
    {
        $this->matcher = new ArgumentsCaptor();
    }

    public function testMatcherGatherAllArguments()
    {
        $this->matcher->match([]);
        $this->matcher->match(['a']);
        $this->matcher->match([2,3]);

        $result = $this->matcher->getValues();

        $expectedResult = [
            [],
            ['a'],
            [2,3]
        ];
        Assert::assertEquals($expectedResult, $result);
    }

}
