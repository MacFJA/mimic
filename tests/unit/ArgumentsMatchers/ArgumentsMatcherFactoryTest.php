<?php
namespace Gstarczyk\Mimic\UnitTest\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatchers\AnyArguments;
use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsCaptor;
use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\ArgumentsMatchers\EmptyArguments;
use Gstarczyk\Mimic\ArgumentsMatchers\MatchingValues;
use Gstarczyk\Mimic\ValueMatchers\AnyStringMatcher;
use PHPUnit_Framework_Assert as Assert;

class ArgumentsMatcherFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var ArgumentsMatcherFactory */
    private $factory;

    protected function setUp()
    {
        $this->factory = new ArgumentsMatcherFactory();
    }

    public function testFactoryReturnAnyArgumentsMatcherWhenNullGiven()
    {
        $result = $this->factory->createMatcher(null);

        Assert::assertInstanceOf(AnyArguments::class, $result);
    }

    public function testFactoryReturnEmptyArgumentsMatcherWhenEmptyArrayGiven()
    {
        $result = $this->factory->createMatcher([]);

        Assert::assertInstanceOf(EmptyArguments::class, $result);
    }

    public function testCreateMatcherFromArguments()
    {
        $values = [
            100
        ];
        $matcher = $this->factory->createMatcher($values);

        $this->assertInstanceOf(MatchingValues::class, $matcher);
    }

    public function testCreateArgumentCaptor()
    {
        $values = [
            new ArgumentsCaptor(),
        ];
        $matcher = $this->factory->createMatcher($values);

        $this->assertInstanceOf(ArgumentsCaptor::class, $matcher);
    }

    /**
     * @param array $data
     * @dataProvider invalidDataForCaptorProvider
     */
    public function testFactoryThrowExceptionWhenGivenCaptorHasAnySiblings(array $data)
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->factory->createMatcher($data);
    }

    /**
     * @return array
     */
    public function invalidDataForCaptorProvider()
    {
        return [
            [
                [
                    new ArgumentsCaptor(),
                    new AnyStringMatcher(),
                ],
            ],
            [
                [
                    new AnyStringMatcher(),
                    new ArgumentsCaptor(),
                ],
            ],
            [
                [
                    new ArgumentsCaptor(),
                    new ArgumentsCaptor(),
                ],
            ],
            [
                [
                    new ArgumentsCaptor(),
                    'some text'
                ],
            ],
        ];
    }

}
