<?php
namespace Gstarczyk\Mimic\UnitTest\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatchers\MatchingValues;
use Gstarczyk\Mimic\ValueMatcher;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class MatchingValuesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param array $matchers
     * @param array $arguments
     * @dataProvider differentSizesProvider
     */
    public function testMatcherReturnFalseWhenArgumentsSizeIsDifferentThanMatchersSize(array $matchers, array $arguments)
    {
        $matcher = new MatchingValues($matchers);
        $result = $matcher->match($arguments);

        Assert::assertFalse($result);
    }

    public function differentSizesProvider()
    {
        return [
            [
                [],
                [1]
            ],
            [
                $this->createMatchers([true]),
                []
            ],
            [
                $this->createMatchers([true]),
                [1, 3]
            ],
            [
                $this->createMatchers([true, true, true]),
                [1]
            ],
        ];
    }

    public function testMatcherReturnTrueWhenAllValueMatchersReturnTrue()
    {
        $matchers = $this->createMatchers([true, true, true]);
        $arguments = [1, 'text', null];

        $matcher = new MatchingValues($matchers);
        $result = $matcher->match($arguments);

        Assert::assertTrue($result);
    }

    public function testMatcherReturnFalseWhenOneOfValueMatchersReturnFalse()
    {
        $matchers = $this->createMatchers([true, true, false]);
        $arguments = [1, 'text', null];

        $matcher = new MatchingValues($matchers);
        $result = $matcher->match($arguments);

        Assert::assertFalse($result);
    }

    /**
     * @param bool $result
     * @return ValueMatcher
     */
    private function createMatcher($result)
    {
        /** @var ValueMatcher | MockObject $matcher */
        $matcher = $this->getMockBuilder(ValueMatcher::class)->getMock();
        $matcher->method('match')->willReturn($result);

        return $matcher;
    }

    /**
     * @param array $values
     * @return array
     */
    private function createMatchers(array $values)
    {
        $matchers = [];
        foreach ($values as $value) {
            $matchers[] = $this->createMatcher($value);
        }

        return $matchers;
    }
}
