<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Import;
use Gstarczyk\Mimic\MockInitiator\ImportFactory;
use PHPUnit_Framework_Assert as Assert;

class ImportFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var ImportFactory */
    private $factory;

    protected function setUp()
    {
        $this->factory = new ImportFactory();
    }

    public function testFactoryReturnImportInstance()
    {
        $result = $this->factory->createImport('A');

        Assert::assertInstanceOf(Import::class, $result);
    }

    public function testFactorySplitFullClassNameIntoNamespaceAndShortName()
    {
        $result = $this->factory->createImport('\MyNamespace\MyClass');

        Assert::assertEquals('\MyNamespace', $result->getNamespace());
        Assert::assertEquals('MyClass', $result->getShortClassName());
    }

    public function testFactoryUseAlias()
    {
        $result = $this->factory->createImport('\MyNamespace\MyClass', 'A');

        Assert::assertEquals('A', $result->getAlias());
    }

    public function testFactoryCanHandleFullClassNameWithoutLeadingBackslash()
    {
        $result = $this->factory->createImport('MyNamespace\MyClass');

        Assert::assertEquals('\MyNamespace', $result->getNamespace());
        Assert::assertEquals('MyClass', $result->getShortClassName());
    }

    /**
     * @param string $fullClassName
     * @dataProvider globalNamespaceClassesProvider
     */
    public function testFactoryCanHandleClassesInGlobalNamespace($fullClassName)
    {
        $result = $this->factory->createImport($fullClassName);

        Assert::assertEquals('\\', $result->getNamespace());
        Assert::assertEquals('MyClass', $result->getShortClassName());
    }

    public function globalNamespaceClassesProvider()
    {
        return [
            ['MyClass'],
            ['\MyClass'],
        ];
    }
}
