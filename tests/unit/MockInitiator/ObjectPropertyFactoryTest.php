<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Context;
use Gstarczyk\Mimic\MockInitiator\ObjectProperty;
use Gstarczyk\Mimic\MockInitiator\ObjectPropertyFactory;
use Gstarczyk\Mimic\MockInitiator\TypeResolver;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use ReflectionProperty;

class ObjectPropertyFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ObjectPropertyFactory */
    private $factory;

    /** @var TypeResolver | MockObject */
    private $typeResolver;

    protected function setUp()
    {
        $this->typeResolver = $this->createMock(TypeResolver::class);
        $this->factory = new ObjectPropertyFactory(
            $this->typeResolver
        );
    }

    public function testFactoryReturnObjectPropertyInstance()
    {
        $reflection = $this->createReflectionPropertyMock('');
        $context = new Context(new \stdClass(), '', []);

        $result = $this->factory->createObjectProperty($reflection, $context);

        Assert::assertInstanceOf(ObjectProperty::class, $result);
    }

    public function testFactoryUseTypeResolverToSetProperType()
    {
        $this->typeResolver->expects($this->once())
            ->method('resolveType');

        $reflection = $this->createReflectionPropertyMock(
            '/** @var string */'
        );
        $context = new Context(new \stdClass(), '', []);

        $this->factory->createObjectProperty($reflection, $context);
    }

    public function testFactoryMarkMockProperties()
    {
        $reflection = $this->createReflectionPropertyMock(
            '/** @mock */'
        );
        $context = new Context(new \stdClass(), '', []);

        $result = $this->factory->createObjectProperty($reflection, $context);

        Assert::assertTrue($result->isMarkedAsMock());
    }

    public function testFactoryMarkMocksTargetProperties()
    {
        $reflection = $this->createReflectionPropertyMock(
            '/** @injectMocks */'
        );
        $context = new Context(new \stdClass(), '', []);

        $result = $this->factory->createObjectProperty($reflection, $context);

        Assert::assertTrue($result->isMarkedAsMocksTarget());
    }

    /**
     * @param string $docComment
     * @return MockObject|ReflectionProperty
     */
    private function createReflectionPropertyMock($docComment)
    {
        $reflection = $this->createMock(ReflectionProperty::class);
        $reflection->method('getDocComment')
            ->willReturn($docComment);

        return $reflection;
    }
}
