<?php
namespace Gstarczyk\Mimic\UnitTest\VerifierBuilder\ConsecutiveInvocations;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations\VerificationException;
use Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations\VerifierBuilder;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class VerifierBuilderTest extends \PHPUnit_Framework_TestCase
{
    const METHOD_NAME = 'someMethod';

    /** @var VerifierBuilder */
    private $builder;

    /** @var InvocationRegistry | MockObject */
    private $invocationRegistryMock;

    /** @var ArgumentsMatcherFactory | MockObject */
    private $argumentsMatcherFactory;

    /** @var ArgumentsMatcher | MockObject */
    private $argumentsMatcher;

    protected function setUp()
    {
        $this->invocationRegistryMock = $this->createMock(InvocationRegistry::class);
        $this->argumentsMatcherFactory = $this->createMock(ArgumentsMatcherFactory::class);

        $this->builder = new VerifierBuilder($this->invocationRegistryMock, $this->argumentsMatcherFactory);
        $this->builder->setMethodName(self::METHOD_NAME);

        $this->argumentsMatcher = $this->createMock(ArgumentsMatcher::class);
    }

    public function test_WasCalledWith_UseMatchingArgumentsFactory()
    {
        $this->setupInvocationRegistryWith(
            [
                new InvocationSignature(self::METHOD_NAME, [10, 'a']),
            ]
        );
        $this->setupArgumentsMatcherToReturnTrue();

        $this->argumentsMatcherFactory->expects($this->once())
            ->method('createMatcher')
            ->with([10, 'a'])
            ->willReturn($this->argumentsMatcher);

        $this->builder->wasCalledWith(10, 'a');
    }

    public function test_WasCalledWith_ThrowException_WhenWasNoInvocationForMethod()
    {
        $this->setupInvocationRegistryWith([]);
        $this->setupArgumentsMatcherFactory();
        $this->setupArgumentsMatcherToReturnTrue();

        $this->expectException(VerificationException::class);

        $this->builder->wasCalledWith(10, 'a');
    }


    public function test_WasCalledWith_ThrowException_WhenArgumentsNotMatch()
    {
        $this->setupInvocationRegistryWith(
            [
                new InvocationSignature(self::METHOD_NAME, [10, 'a']),
            ]
        );
        $this->setupArgumentsMatcherFactory();
        $this->setupArgumentsMatcherToReturnFalse();

        $this->expectException(VerificationException::class);

        $this->builder->wasCalledWith(10);
    }

    private function setupInvocationRegistryWith(array $invocations)
    {
        $this->invocationRegistryMock->expects($this->any())
            ->method('getMethodInvocations')
            ->willReturn($invocations);
    }

    private function setupArgumentsMatcherToReturnTrue()
    {
        $this->argumentsMatcher->expects($this->any())
            ->method('match')
            ->willReturn(true);
    }

    private function setupArgumentsMatcherToReturnFalse()
    {
        $this->argumentsMatcher->expects($this->any())
            ->method('match')
            ->willReturn(false);
    }

    private function setupArgumentsMatcherFactory()
    {
        $this->argumentsMatcherFactory->expects($this->any())
            ->method('createMatcher')
            ->willReturn($this->argumentsMatcher);

    }
}
