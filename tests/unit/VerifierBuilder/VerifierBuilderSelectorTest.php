<?php
namespace Gstarczyk\Mimic\UnitTest\VerifierBuilder;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;
use Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;
use Gstarczyk\Mimic\VerifierBuilder\VerifierBuilderSelector;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class VerifierBuilderSelectorTest extends \PHPUnit_Framework_TestCase
{
    /** @var VerifierBuilderSelector */
    private $selector;

    /** @var InvocationRegistry | MockObject */
    private $invocationRegistry;

    /** @var ArgumentsMatcherFactory | MockObject */
    private $argumentsMatcherFactory;

    protected function setUp()
    {
        $this->argumentsMatcherFactory = $this->createMock(ArgumentsMatcherFactory::class);
        $this->invocationRegistry = $this->createMock(InvocationRegistry::class);
        $this->selector = new VerifierBuilderSelector($this->invocationRegistry, $this->argumentsMatcherFactory);
    }

    public function testMethodReturnFilteredInvocationVerifier()
    {
        $result = $this->selector->method('someMethod');

        Assert::assertInstanceOf(FilteredInvocations\VerifierBuilder::class, $result);
    }

    public function testConsecutiveMethodInvocationsReturnConsecutiveInvocationVerifier()
    {
        $result = $this->selector->consecutiveMethodInvocations('someMethod');

        Assert::assertInstanceOf(ConsecutiveInvocations\VerifierBuilder::class, $result);
    }
}
