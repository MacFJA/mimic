<?php
namespace Gstarczyk\Mimic\IntegrationTest\Stub\BehaviourBuilder\ConsecutiveInvocations;

use Gstarczyk\Mimic\InvocationHandler;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\Stub\BehaviourBuilder\ConsecutiveInvocations\BehaviourBuilder;
use Gstarczyk\Mimic\Stub\Stub;
use PHPUnit_Framework_Assert as Assert;

class BehaviourBuilderTest extends \PHPUnit_Framework_TestCase
{
    /** @var BehaviourBuilder */
    private $builder;

    /** @var InvocationHandler */
    private $invocationHandler;

    protected function setUp()
    {
        $invocationRegistry = new InvocationRegistry();
        $stub = new Stub($invocationRegistry);

        $this->builder = new BehaviourBuilder($stub);
        $this->invocationHandler = new InvocationHandler($invocationRegistry, $stub);
    }

    public function testBuildBehaviour()
    {
        $this->builder->setMethodName('someMethod');
        $this->builder->willReturn('first');
        $this->builder->thenReturnCallbackResult(function ($arg1) {
            return $arg1;
        });
        $this->builder->thenReturn('third');

        $result1 = $this->invocationHandler->handleInvocation('someMethod', []);
        $result2 = $this->invocationHandler->handleInvocation('someMethod', ['second']);
        $result3 = $this->invocationHandler->handleInvocation('someMethod', []);

        Assert::assertEquals('first', $result1);
        Assert::assertEquals('second', $result2);
        Assert::assertEquals('third', $result3);
    }
}
