<?php
namespace Gstarczyk\Mimic\IntegrationTest\Mock;

use Gstarczyk\Mimic\IntegrationTest\Fixture\ClassWithTrait;
use Gstarczyk\Mimic\Mock\ClassDefinition;
use Gstarczyk\Mimic\Mock\ClassDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use Gstarczyk\Mimic\UniqueIdGenerator;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassOne;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class ClassDefinitionFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var ClassDefinitionFactory */
    private $factory;

    /** @var UniqueIdGenerator | MockObject */
    private $uniqueIdGeneratorMock;

    /** @var MethodArgumentFactory */
    private $methodArgumentFactory;

    protected function setUp()
    {
        $this->uniqueIdGeneratorMock = $this->createMock(UniqueIdGenerator::class);
        $this->methodArgumentFactory = new MethodArgumentFactory();
        $this->factory = new ClassDefinitionFactory(
            $this->uniqueIdGeneratorMock,
            $this->methodArgumentFactory
        );

        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');
    }

    public function testFactoryCreateClassDefinitionObject()
    {
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertInstanceOf(ClassDefinition::class, $definition);

        $expected = file_get_contents(__DIR__ . '/../Fixture/ClassOne_mock_code.txt');
        $this->assertEquals($expected, $definition->toCode());
    }

    public function testCreateClassDefinitionForEmptyClassUsingTrait()
    {
        $definition = $this->factory->createClassDefinition(ClassWithTrait::class);

        Assert::assertInstanceOf(ClassDefinition::class, $definition);

        $expected = file_get_contents(__DIR__ . '/../Fixture/ClassWithTrait_mock_code.txt');
        $this->assertEquals($expected, $definition->toCode());
    }
}
