<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

interface InterfaceToMock
{
    public function methodOne();
}