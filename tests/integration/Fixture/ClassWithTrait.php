<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

class ClassWithTrait
{
    use MyTrait;
}

trait MyTrait
{
    public function traitMethod()
    {

    }
}